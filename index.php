<?php 

/* composer */
include __DIR__ . '/vendor/autoload.php';

/* load config */
include __DIR__ . '/app/config/config.php';

/* load routing */
include __DIR__ . '/app/route/web.php';

/* load app */
include __DIR__ . '/app/service/main.php';

?>