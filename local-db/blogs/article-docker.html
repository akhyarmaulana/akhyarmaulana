<h2>Introduction to Docker</h2>

<p>In the software industry, we often face environment-related issues. An environment, in this context, includes all the software dependencies a program has on the computer it runs on. More specifically, it involves software versioning, configuration, and deployment methods.</p>

<p>These challenges have been addressed by a tool called Docker. Docker packages an environment into an image. This image can be run on its own or alongside other images, creating an ideal environment for software development.</p>

<p>For example, suppose your laptop has PHP version 7.3 installed, but the program you're developing requires PHP version 8.1. Normally, you would update PHP on your laptop to the newer version. But what if you later need to use the older PHP version again? Will you constantly switch between upgrading and downgrading? What about NPM or MySQL? If each project has different requirements, managing these dependencies becomes quite challenging.</p>

<p>The answer to these questions is Docker. With Docker, you can manage PHP versions for each project individually. And it’s not limited to PHP; you can also configure database versions, web servers, and more.</p>

<h2>Let's Go</h2>

<p>In Docker, there are a few essential terms to understand:</p>

<ul>
    <li>Docker File</li>
    <li>Docker Image</li>
    <li>Docker Composer</li>
</ul>

<h3>Docker File</h3>

<p>A Dockerfile serves as a bridge between users and Docker. Users can specify everything they want to include in an image. The code in a Dockerfile is similar to shell scripting, using commands much like those in a Linux operating system, along with specific Dockerfile instructions such as FROM, EXPOSE, and RUN.</p>

<ul>
    <li>FROM is used to retrieve a base image from another source.</li>
    <li>EXPOSE is used to open ports for the container.</li>
    <li>RUN is used to execute commands.</li>
</ul>

<p>Here's an example of a Dockerfile for a PHP environment on Alpine Linux with installed extensions for PHP GD, PHP Redis, and PHP PDO MySQL:</p>

<pre>
# Dockerfile Example
FROM alpine:latest
RUN apk add --no-cache php php-gd php-redis php-pdo_mysql
EXPOSE 80
</pre>

<p>Save the file with the name Dockerfile. At this stage, the Dockerfile is not yet an image. To turn it into an image, it needs to go through one more process, called building. Here’s how to build and tag the image:</p>

<ul>
    <li>Open a terminal and navigate to the directory containing the Dockerfile.</li>
    <li>Run the following command to build the image and assign it a tag:
    </li>
</ul>

<pre>
    docker build -t your_image_name:tag .
</pre>

<h3>Docker Image</h3>

<p>We can think of a Docker image as an environment image. Just as we can create a CD or DVD image file (like .iso files), we can also create an environment image using Dockerfiles and Docker images. This captures the essence of Docker's purpose: to enable the creation and management of consistent, portable environment images.</p>

<p>Previously, we created a Docker image from a Dockerfile. We can view the images on our computer with the following command:</p>

<pre>
    docker images
</pre>

<p>This command lists all the Docker images currently available on your system, showing details like repository, tag, image ID, and size.</p>

<p>In addition to the images we create with a Dockerfile, we can also download pre-existing images from Docker Hub. Here is the link to Docker Hub if you want to browse available images:</p>

<a href="https://hub.docker.com/" target="_blank" rel="noopener">Docker Hub</a>

<p>You can search for a variety of images based on different applications, programming languages, and frameworks to use in your projects.</p>

<h3>Docker Compose</h3>

<p>From the Docker images we have created, we can combine them with images available on Docker Hub and configure them according to our needs. Each image will have different settings, but typically, the image creator will provide documentation on how to use it. You can follow that documentation or ask questions in groups or forums.</p>

<p>Below is an example of my Docker Compose file for a development environment using Go, PostgreSQL, PgAdmin, and Redis:</p>

<pre>
version: '3' 
services:
  postgres:
    image: postgres:10.4
    ports:
      - "5431:5432"
    environment:
      - POSTGRES_USER=postgres
      - POSTGRES_PASSWORD=apple
      - POSTGRES_DB=app 
    volumes:
      - ./db_data/postgres:/var/lib/postgresql/data

  pgadmin:
    image: dpage/pgadmin4
    environment:
      - PGADMIN_DEFAULT_EMAIL=admin@pangeranweb.com
      - PGADMIN_DEFAULT_PASSWORD=apple
    ports:
      - "8889:80"
    depends_on:
      - postgres
    logging:
      driver: "none"

  web:
    image: akhyarmaulana/golang14
    ports:
      - "8001:8080"
    volumes:
      - ./:/go
    depends_on:
      - postgres
      - redis
    tty: true
    command: sh /go/deploy.sh

  redis:
    image: redis
    ports:
      - "6370:6379"
</pre>

<p>In this example, the docker-compose.yml file defines services for PostgreSQL, PgAdmin, a Go web application, and Redis, along with their configurations and dependencies.</p>

<p>Isn't it exciting? Let's start learning Docker!</p>