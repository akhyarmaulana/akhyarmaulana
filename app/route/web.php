<?php 


/* web routing */

$routes['get.home'] = [
    'url' => '',
    'controller' => "app/controller/home_controller.php"
];

$routes['get.blogs'] = [
    'url' => 'blogs',
    'controller' => "app/controller/blogs_controller.php"
];

$routes['get.blogs.detail'] = [
    'url' => 'blog/[any]',
    'controller' => "app/controller/blogs_detail_controller.php"
];

$routes['sitemap'] = [
    'url' => 'sitemap-xml',
    'controller' => "app/controller/sitemap_controller.php"
];