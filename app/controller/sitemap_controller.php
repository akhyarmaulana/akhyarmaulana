<?php 

$blog_index = loadLocalDb('blogs/index');

$sites = [];

$sites[] = [
    'link' => route('get.home'),
    'time' => date("Y-m-d H:i:s")
];
$sites[] = [
    'link' => route('get.blogs'),
    'time' => date("Y-m-d H:i:s")
];

foreach ($blog_index['blogs_index'] as $v) {
    $sites[] = [
        'link' => route('get.blogs.detail', slug($v['title'])),
        'time' => date("Y-m-d H:i:s")
    ];
}

$data['sites'] = $sites;

render('pages/sitemap', $data);

