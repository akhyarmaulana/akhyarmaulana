<?php 

include __DIR__ . "/../service/connection.php";

$setting_res = multiplequery("SELECT * FROM settings");

mysqli_close($GLOBALS['conn']);

$setting = [];
foreach ($setting_res as $v) {
    $setting[$v['setting_key']] = $v['setting_value'];
}

$index_data = loadLocalDb('blogs/index');
$blog = [];
foreach ($index_data['blogs_index'] as $v) {
    if (slug($v['title']) == $GLOBALS['param']) {
        $blog = $v;
    }
}

$path = __DIR__ . '/../../' . $blog['article'];
$blog['main_html'] = file_get_contents($path);

$data = [];
$data['blog'] = $blog;
$data['blog_index'] = $index_data;
$data['setting'] = $setting;

render("pages/blog/blogs-detail", $data);

?>