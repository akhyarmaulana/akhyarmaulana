<?php 



include __DIR__ . "/../service/connection.php";

$setting_res = multiplequery("SELECT * FROM settings");

mysqli_close($GLOBALS['conn']);


$blog_index = loadLocalDb('blogs/index');

/* filter */
$blog = [];
foreach ($blog_index['blogs_index'] as $v) {
    if (!empty($_GET['search'])) {
        if (checkWordsExist()) {
            $blog[] = $v;
        }
    }
    
    if (!empty($_GET['cat'])) {
        if (in_array($_GET['cat'], $v['tags'])) {
            $blog[] = $v;
        }
    }

    if (empty($_GET['search']) && empty($_GET['cat'])) {
        $blog[] = $v;
    }
}


$setting = [];
foreach ($setting_res as $v) {
    $setting[$v['setting_key']] = $v['setting_value'];
}

$data = [];
// $data['experience'] = $experience;
$data['blog'] = $blog;
$data['setting'] = $setting;
$data['blog_index'] = $blog_index;

render("pages/blog/blogs", $data);

?>