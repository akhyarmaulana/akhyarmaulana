<?php 

/* load lib */
include __DIR__ . "/../service/connection.php";


/* do query */
$bio_res = multiplequery("SELECT * FROM pages WHERE page_location = 'home' and page_section like '%bio%'");
$stack_res = multiplequery("SELECT * FROM pages WHERE page_location = 'home' and page_section like '%stack%'");
$works_res = multiplequery("SELECT * FROM pages WHERE page_location = 'home' and page_section = 'works'");
$experience_res = multiplequery("SELECT * FROM pages WHERE page_location = 'home' and page_section = 'experience'");
$education_res = multiplequery("SELECT * FROM pages WHERE page_location = 'home' and page_section = 'education' ORDER BY id DESC");
$setting_res = multiplequery("SELECT * FROM settings");


/* arrage views variable */
$bio = [];
foreach ($bio_res as $v) {
    $bio[$v['page_section']] = $v;
}

$stack = [];
foreach ($stack_res as $v) {
    if ($v['page_section'] == "stack_info") {
        $stack['info'] = $v['page_value'];
    } else if ($v['page_section'] == "stack_info_small") {
        $stack['info_small'] = $v['page_value'];
    } else {
        $stack['pictures'][] = uri($v['page_value']);
    }
}

$works = [];
foreach ($works_res as $v) {
    $items = multiplequery("SELECT * FROM page_items WHERE pages_id = '".$v['id']."'");
    $v['content'] = [];
    foreach ($items as $i) {
        $v['content'][$i['meta_key']] = $i['meta_value'];
    }   
    $works[] = $v;
}

$experience = [];
foreach ($experience_res as $v) {
    $items = multiplequery("SELECT * FROM page_items WHERE pages_id = '".$v['id']."'");
    $v['content'] = [];
    foreach ($items as $i) {
        $v['content'][$i['meta_key']] = $i['meta_value'];
    }   
    $experience[] = $v;
}


$educations = [];
foreach ($education_res as $v) {
    $items = multiplequery("SELECT * FROM page_items WHERE pages_id = '".$v['id']."'");
    $v['content'] = [];
    foreach ($items as $i) {
        $v['content'][$i['meta_key']] = $i['meta_value'];
    }   
    $educations[] = $v;
}

/* close connection to database after done */
mysqli_close($GLOBALS['conn']);

$setting = [];
foreach ($setting_res as $v) {
    $setting[$v['setting_key']] = $v['setting_value'];
}

/* render the views */
$data['bio'] = $bio;
$data['stack'] = $stack;
$data['works'] = $works;
$data['experience'] = $experience;
$data['educations'] = $educations;
$data['setting'] = $setting;

// debug($data);

render("pages/home", $data);