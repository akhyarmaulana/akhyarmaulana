<?php 

function singlequery($query) 
{
    $result = mysqli_query($GLOBALS['conn'], $query);
    if (!$result) {
        echo mysqli_error($GLOBALS['conn']); exit();
    }
    $data = mysqli_fetch_assoc($result);
    

    return $data;
}

function multiplequery($query)
{
    $result = mysqli_query($GLOBALS['conn'], $query);
    if (!$result) {
        echo mysqli_error($GLOBALS['conn']); exit();
    }
    $res = [];
    while($data = mysqli_fetch_assoc($result)) {
        $res[] = $data;
    }

    return $res;
}

function uri($url = '')
{
    return BASE_URL . "/" .  $url;
}

function render($target_page, Array $data = [])
{
    /* array key as new variable */
    foreach ($data as $k => $v) {
        ${$k} = $v;
    }

    include __DIR__ . "/../../views/" . $target_page . ".php";
}

function dd($var)
{
    echo "<pre>";
    var_dump($var);
    echo "</pre>";
    exit();
}

function activeurl()
{
    $current_url = getCurrentUrl();
    $uri = uri();
    return str_replace($uri, '', $current_url);
}


function getCurrentUrl() 
{
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    $host = $_SERVER['HTTP_HOST'];
    $host = str_replace('www.','', $host);
    $uri = $_SERVER['REQUEST_URI'];
    return $protocol . $host . $uri;
}

function route($name, $param = "")
{
    if (!empty($GLOBALS['routers'][$name])) {
        if (!empty($param)) {
            $url = $GLOBALS['routers'][$name]['url'];
            $url = str_replace('[any]', $param, $url);
            return uri($url);
        } else {
            return uri($GLOBALS['routers'][$name]['url']);
        }
    } else {
        return null;
    }
    
}


function loadLocalDb($target_file)
{
    $data = file_get_contents(__DIR__ . '/../../local-db/' . $target_file . '.json');
    return json_decode($data, true);
}

function checkWordsExist($needle, $longString) {
    // Convert needle to an array of words if it's a single word or a space-separated string
    $words = is_array($needle) ? $needle : explode(" ", $needle);
    
    foreach ($words as $word) {
        if (strpos($longString, $word) !== false) {
            return true; // Word found
        }
    }
    
    return false; // No words found
}

function slug($string) {
    // Convert to lowercase
    $slug = strtolower($string);
    
    // Remove any non-alphanumeric characters except spaces
    $slug = preg_replace('/[^a-z0-9\s-]/', '', $slug);
    
    // Replace multiple spaces or hyphens with a single hyphen
    $slug = preg_replace('/[\s-]+/', '-', $slug);
    
    // Trim hyphens from the beginning and end
    $slug = trim($slug, '-');
    
    return $slug;
}
?>