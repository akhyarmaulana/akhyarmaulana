<?php 

include __DIR__ . '/functions.php';


function app($routers)
{
    $GLOBALS['routers'] = $routers;
    $current_url = getCurrentUrl();
    $uri = uri();

    $app_url = str_replace($uri, '', $current_url);
    $query_param = explode("?", $app_url);
    if (!empty($query_param[1])) {
        $app_url = $query_param[0];
    }
    

    foreach ($routers as $route_name => $v) {
        /* direct url */
        // dd($app_url);
        if ($v['url'] == $app_url) {
            /* load controller */
            
            include __DIR__ . '/../../' . $v['controller'];
        }

        /* dinamic url */
        if ($route_name == 'get.blogs.detail') {
            $dinamic_url = $v['url'];
            $dinamic_url = str_replace('[any]', '', $dinamic_url);
            $param_url = str_replace($dinamic_url, '', $app_url);
            if ($param_url != $app_url) {
                /* route selected */
                $GLOBALS['param'] = $param_url;
                
                /* load controller */
                include __DIR__ . '/../../' . $v['controller'];
            }
        }
    
    }
}

app($routes);