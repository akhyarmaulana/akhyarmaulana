<?php
$seo = [
    'title' => $blog['title'],
    'description' => $blog['sort_description'],
    'keyword' => $blog['keyword'],
    'image' => uri($blog['featured_image'])
];
include __DIR__ . "/../../sections/header-template.php";
include __DIR__ . "/../../sections/nav.php";
?>

<section class="container-fluid dot-pattern" id="bio">
    <section class="container about-title-container">
        <div class="row">
            
            <div class="col-12 col-sm-12 col-md-9">
                <div class="article-main-container">
                    <img src="<?=uri($blog['featured_image'])?>" alt="<?=$blog['title']?>"  title="<?=$blog['title']?>" class="w-100">
                    
                </div>
                <div class="row justify-content-center">
                    <div class="col-12 col-sm-12 col-md-8 col-lg-8 mt-5">
                        <h1 class="blog-mian-title"><?=$blog['title']?></h1>
                        <h6><?=$blog['timly']?></h6>
                        <?php foreach ($blog['tags'] as $vt) : ?>
                        <small class="badge bg-secondary"><?=ucwords($vt)?></small>
                        <?php endforeach ?>
                        <div class="main-article mt-4">
                            <?= $blog['main_html'] ?>
                        </div>

                        
                    </div>
                    <div class="col-12">
                        <div class="share-container mt-5 mb-5">
                            <div><small>Share this article</small></div>
                            <a href="#" class="btn btn-primary btn-share">
                                <i class="fa fa-facebook"></i> Facebook Share
                            </a>
                            <a href="#" class="btn btn-info btn-share">
                                <i class="fa fa-twitter"></i> Twitter Share
                            </a>
                            <a href="#" class="btn btn-success btn-share">
                                <i class="fa fa-whatsapp"></i> Whatsapp Share
                            </a>
                            <a href="#" class="btn btn-warning btn-share">
                                <i class="fa fa-link"></i> Link Copy
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 col-sm-12 col-md-3">
                <?php include __DIR__ . "/blog-nav.php"; ?>
            </div>

        </div>
    </section>
</section>

<?php

include __DIR__ . "/../../sections/footer.php";
include __DIR__ . "/../../sections/footer-template.php";

?>