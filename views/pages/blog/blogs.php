<?php

$seo = [
    'title' => "Akhyar Maulana | Welcome to My Blogs",
    'description' => 'Visit our blog for fresh, engaging content that explores tech, software, and industry insights from a unique angle. Each article is crafted to inspire new perspectives, challenge familiar viewpoints, and spark curiosity. Discover thought-provoking topics that go beyond the ordinary and bring new understanding to everyday ideas.',
    'keyword' => 'Akhyar maulana , akhyar blog, web developer, belajar koding, belajar server, belajar backend',
    'image' => uri($blog[0]['featured_image'])
];
include __DIR__ . "/../../sections/header-template.php";
include __DIR__ . "/../../sections/nav.php";


?>

<section class="container-fluid dot-pattern" id="bio">
    <section class="container about-title-container">
        <div class="row">
            <div class="col-12 text-center mb-4">
                <h1 class="aboutus-title" id="bio" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="300">Welcome to My Blogs</h1>
            </div>
            <div class="col-12 col-sm-12 col-md-4">
                <?php include __DIR__ . "/blog-action.php"; ?>
            </div>
            <div class="col-12 col-sm-12 col-md-8">
                <div class="row">
                    <?php foreach($blog as $k => $v) : ?>
                    <div class="col-md-6">
                        <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="700">
                            <a href="<?= route('get.blogs.detail', slug($v['title'])) ?>"><img src="<?=uri($v['featured_image'])?>" class="card-img-top blog-image" alt="<?=$v['title']?>"></a>
                            <div class="card-body case-study-card-body">
                                <?php foreach ($v['tags'] as $vt) : ?>
                                <small class="badge bg-secondary"><?=ucwords($vt)?></small>
                                <?php endforeach ?>
                                <a href="<?= route('get.blogs.detail', slug($v['title'])) ?>"><h5 class="card-title blog-title text-black mt-1"><?=$v['title']?></h5></a>
                                <h6 class="card-subtitle mb-2 text-muted"></h6>
                                <p class="card-text blog-desc">
                                <?=ucwords($v['sort_description'])?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                
                </div>
            </div>
        </div>
    </section>
</section>

<?php

include __DIR__ . "/../../sections/footer.php";
include __DIR__ . "/../../sections/footer-template.php";

?>