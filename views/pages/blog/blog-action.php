<form action="" method="get" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="300">
    <div class="input-group mb-3">
        <input type="text" class="form-control custom-search-input" placeholder="Search post...">
        <div class="input-group-append">
            <button class="btn btn-outline-default custom-search-button" type="button">
                <i class="fa fa-search"></i>
            </button>
        </div>
    </div>
</form>

<ul class="list-group category-list" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="500">
    <li class="list-group-item category-list-title">
        <b><i class="fa fa-tags"></i> Main Categories</b>
    </li>
    <li class="list-group-item">
        <a href="<?=route('get.blogs')?>"><i class="fa fa-tag"></i> All</a>
    </li>
    <?php foreach($blog_index['tags_index'] as $v) : ?>
    <li class="list-group-item <?= (!empty($_GET['cat']) && $v == $_GET['cat'] ? 'active' : '')  ?>">
        <a href="?cat=<?=$v?>"><i class="fa fa-tag"></i> <?= ucfirst($v) ?></a>
    </li>
    <?php endforeach ?>
    
</ul>

<div class="list-group mt-4 mb-4 blog-lates-article" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="600">
    <a class="list-group-item list-group-item-action flex-column align-items-start category-list-title">                        
        <h3 class="mt-2 text-white"><i class="fa fa-refresh"></i> Latest Update Article</h3>
    </a>
    
    <?php foreach ($blog_index['blogs_index'] as $v) : ?>
    <?php 
    if (!$v['show_on_lastes_update']) {
        continue;
    }
    ?>
    <a href="<?= route('get.blogs.detail', slug($v['title'])) ?>" class="blog-lates-list list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100 justify-content-between">
        <h5 class="mb-1"><?= $v['title'] ?></h5>
        <small class="text-muted"><?= $v['timly'] ?></small>
        </div>
        <p class="mb-1"><?= $v['sort_description'] ?></p>
        <?php foreach ($v['tags'] as $vs) : ?>
            <small class="text-muted badge bg-silver">#<?= ucfirst($vs) ?></small>
        <?php endforeach; ?>
        
    </a>
    <?php endforeach ?>
    
</div>