<?php

include __DIR__ . "/../sections/header-template.php";
include __DIR__ . "/../sections/nav.php";
include __DIR__ . "/../sections/bio.php";
include __DIR__ . "/../sections/stack.php";
include __DIR__ . "/../sections/works.php";
include __DIR__ . "/../sections/experience.php";
include __DIR__ . "/../sections/education.php";
include __DIR__ . "/../sections/footer.php";
include __DIR__ . "/../sections/footer-template.php";

?>