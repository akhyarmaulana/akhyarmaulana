<img src="assets/img/table-setup.jpg" class="fix-parralax" >

<section class="case-study-container fluid-container mt-5 pt-3 pb-5" id="my-works">
    <section id="study-case" class="container mt-4">
        <div class="row">
            <div class="col-12">
                <h3 class="text-center text-white my-4" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="400">My Recent Works</h3>
                <div class="title-underline-secondary mb-5" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="700"></div>
            </div>
            <div class="col-12 col-sm-12 col-md-6">
                <?php foreach($works as $k => $v) : ?>
                <?php 
                if ($k%2 != 0) {
                    continue;
                }
                ?>
                <div class="col-md-12">
                    <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="700">
                        <img src="<?=uri($v['content']['image'])?>" class="card-img-top" alt="<?=$v['page_value']?>">
                        <div class="card-body case-study-card-body">
                            <small class="text-primary"><?=ucwords($v['content']['category_tag'])?></small>
                            <h5 class="card-title"><?=$v['page_value']?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?=ucwords($v['content']['desc'])?></h6>
                            <p class="card-text">
                                
                            </p>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                
            </div>
            <div class="col-12 col-sm-12 col-md-6">
                <?php foreach($works as $k => $v) : ?>
                <?php 
                if ($k%2 == 0) {
                    continue;
                }
                ?>
                <div class="col-md-12">
                    <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="700">
                        <img src="<?=uri($v['content']['image'])?>" class="card-img-top" alt="<?=$v['page_value']?>">
                        <div class="card-body case-study-card-body">
                            <small class="text-primary"><?=ucwords($v['content']['category_tag'])?></small>
                            <h5 class="card-title"><?=$v['page_value']?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?=ucwords($v['content']['desc'])?></h6>
                            <p class="card-text">
                                
                            </p>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
</section>