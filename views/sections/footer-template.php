<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
    <script>
    $('.carousel .carousel-item').each(function () {
        var minPerSlide = 4
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < minPerSlide; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
        }
    });
    </script>
    <script>
        var height_windows = ($(window).height());
        var width_window = ($(window).width());
        
        if (width_window > 990) {
            $("#banner").css("height", height_windows + "px");
            $(".super-title").css("margin-top", ((height_windows / 2 ) - 200) + "px");
        }
        
        
    </script>
    <script>
        var height = $(".article-small-card").height();
        $(".card-img-left").css("height",height + "px");
    </script>
    <script>
        let prevScrollPos = window.pageYOffset;
    
        window.onscroll = function() {
        const currentScrollPos = window.pageYOffset;
    
        if (prevScrollPos > currentScrollPos) {
            // Scrolling up
            $(".main-header").removeClass("hidden");
        } else {
            // Scrolling down
            $(".main-header").addClass("hidden");
        }
    
        prevScrollPos = currentScrollPos;
        };
    
    </script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
    <script>
            
            const swiper = new Swiper('.swiper', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
                slidesPerView: 1.5,
                breakpoints: {
                    320: {
                        spaceBetween: 2,
                        spaceBetween: 20,
                    },
                    720: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    992: {
                        slidesPerView: 4,
                        spaceBetween: 30,
                    },
                    2250: {
                        slidesPerView: 4,
                        spaceBetween: 30,
                    },
                }
            });

            const swipertech = new Swiper('.swiper-tech', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
                slidesPerView: 3.5,
                breakpoints: {
                    320: {
                        spaceBetween: 4,
                        spaceBetween: 0,
                    },
                    720: {
                        slidesPerView: 4,
                        spaceBetween: 0,
                    },
                    992: {
                        slidesPerView: 4,
                        spaceBetween: 0,
                    },
                    2250: {
                        slidesPerView: 4,
                        spaceBetween: 0,
                    },
                }
            });            

            const swipernews = new Swiper('.swiper-news', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
                slidesPerView: 1.3,
                breakpoints: {
                    320: {
                        spaceBetween: 20,
                        spaceBetween: 20,
                    },
                    720: {
                        slidesPerView: 20,
                        spaceBetween: 20,
                    },
                    992: {
                        slidesPerView: 30,
                        spaceBetween: 30,
                    },
                    2250: {
                        slidesPerView: 30,
                        spaceBetween: 30,
                    },
                }
            });            

    </script>
    <script>
        /* url check */
        $(".nav-link").click(function(){
            $(".nav-link").removeClass("active");
            setTimeout(() => {
                $(this).addClass("active");
            }, 400);
        });

        /* fragment check */
        var fragment = window.location.hash.substring(1);
        var target = '.nav-' + fragment;
        console.log('target', target);
        $(target).click();
        // setInterval(() => {
        //     const currentUrl = window.location.href;

        //     /* active id # */
        //     const splited = currentUrl.split("#");
        //     if (splited[1]) {
        //         console.log(splited[1]);
        //     }
        // }, 1000);

    </script>
  </body>
</html>