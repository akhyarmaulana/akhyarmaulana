<header class="container-fluid main-header">
    <div class="container center-main-header">
        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
                <a class="navbar-brand" href="/">
                <img src="<?=uri('assets/img/logo-unusual-negative.png')?>" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="50" alt="Akhyar Maulana Logo" title="Akhyar Maulana Logo" class="w-100 main-logo">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-navicon text-white"></span>
                </button>
                <div class="collapse navbar-collapse main-navbar" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="500">
                    <a class="nav-bio nav-link <?= getCurrentUrl() ?>" aria-current="page" href="<?=route('get.home')?>#bio">Bio</a>
                    </li>
                    <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="700">
                    <a class="nav-stack nav-link" href="<?=route('get.home')?>#stack">Stack</a>
                    </li>
                    <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="900">
                    <a class="nav-my-works nav-link " href="<?=route('get.home')?>#my-works">Works</a>
                    </li>
                    <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1200">
                    <a class="nav-experience nav-link " href="<?=route('get.home')?>#experience">Experience</a>
                    </li>
                    <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1400">
                        <a class="nav-educations nav-link" href="<?=route('get.home')?>#educations">Education</a>
                    </li>
                    <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1600">
                        <a class="nav-link <?=activeurl() == 'blogs' ? 'active' : '' ?>" href="<?=route('get.blogs')?>">Blogs <span class="badge bg-warning"><i class="text-black fa fa-book"></i></span> </a>
                    </li>
                </ul>
                <div data-aos="fade-left" data-aos-duration="4000" data-aos-delay="4000" class="action-button">
                    <a class="btn btn-warning my-bg-secondary full-round font-bold" href="https://wa.me/6289671304121"><i class="fa fa-whatsapp"></i> WhatsApp Me</a>
                </div>
                </div>
            </div>
        </nav>
    </div>
</header>


