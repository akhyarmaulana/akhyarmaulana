<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="<?=uri('apple-touch-icon.png')?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?=uri('android-chrome-192x192.png')?>">
    <link rel="icon" type="image/png" sizes="512x512"  href="<?=uri('android-chrome-512x512.png')?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?=uri('favicon-32x32.png')?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?=uri('favicon-16x16.png')?>">
    <link rel="manifest" href="manifest.json">


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="<?=uri('assets/css/about.css?20240515173322')?>">
    <link rel="stylesheet" href="<?=uri('assets/css/style.css')?>">
    
    <?php if (!empty($seo)) : ?>
    <title><?= $seo['title'] ?></title>
    <meta name="title" content="<?= $seo['title'] ?>">
    <meta name="description" content="<?= $seo['description'] ?>">
    <meta name="keywords" content="<?= $seo['keyword'] ?>">
    <meta property="og:title" content="<?= $seo['title'] ?>">
    <meta property="og:description" content="<?= $seo['description'] ?>">
    <meta property="og:image" content="<?= $seo['image'] ?>">
    <meta property="og:url" content="<?=getCurrentUrl()?>" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="<?= $seo['title'] ?>" />
    <meta name="twitter:description" content="<?= $seo['description'] ?>" />
    <meta name="twitter:image" content="<?= $seo['image'] ?>" />
    <meta name="twitter:image:src" content="<?= $seo['image'] ?>" /> 
    <link rel="canonical" href="<?=getCurrentUrl()?>" >
    <?php else : ?>
    <title>Akhyar Maulana | Software Solution</title>
    <meta name="title" content="Akhyar Maulana | Expienced Backend Developer">
    <meta name="description" content="With over a decade of experience in web development, I specialize in full-stack development, with a particular emphasis on backend technologies">
    <meta name="keywords" content="akhyar maulana, jasa programmer, jasa website, pembuatan website, jasa aplikasi, pembuatan aplikasi, app developer, web developer, system developer">
    <meta property="og:title" content="Akhyar Maulana | Solusi Software, Website, dan Aplikasi Mobile">
    <meta property="og:description" content="With over a decade of experience in web development, I specialize in full-stack development, with a particular emphasis on backend technologies">
    <meta property="og:image" content="<?=uri('assets/img/table-setup.jpg')?>">
    <meta property="og:url" content="<?=getCurrentUrl()?>" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Akhyar Maulana | Expienced Backend Developer" />
    <meta name="twitter:description" content="With over a decade of experience in web development, I specialize in full-stack development, with a particular emphasis on backend technologies" />
    <meta name="twitter:image" content="<?=uri('assets/img/table-setup.jpg')?>" />
    <meta name="twitter:image:src" content="<?=uri('assets/img/table-setup.jpg')?>" /> 
    <link rel="canonical" href="https://akhyarmaulana.com" >
    <?php endif; ?>


    <meta name="robots" content="index, follow" />
    <meta name="googlebot" content="index, follow" />
    <meta name="googlebot-news" content="index, follow" />
    <meta name="google-site-verification" content=""/>
    <link rel="dns-prefetch" href="//connect.facebook.net/">
    <link rel="preconnect" href="//connect.facebook.net/" crossorigin>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="mobile-web-app-capable" content='yes' >
    <meta name="apple-touch-fullscreen" content='yes' >
    <meta name="apple-mobile-web-app-capable" content='yes' >
    <meta name="apple-mobile-web-app-status-bar-style" content='default' >
    <meta name="theme-color" content="#0C9344">

    <?php if (!empty($seo)) : ?>
        <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "BlogPosting",
            "headline": "<?= $seo['title'] ?>",
            "description": "<?= $seo['description'] ?>",
            "keywords": [<?= $seo['keyword'] ?>],
            "author": {
                "@type": "Person",
                "name": "Akhyar Maulana"
            },
            "url": "<?=getCurrentUrl()?>",
            "mainEntityOfPage": "<?=getCurrentUrl()?>",
            "datePublished": "2024-11-01", 
            "publisher": {
                "@type": "Organization",
                "name": "Akhyar Maulana",
                "logo": {
                    "@type": "ImageObject",
                    "url": "https://akhyarmaulana.com/assets/img/logo-unusual-negative.png"
                }
            }
        }
        </script>


    <?php else : ?>
        <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "LocalBusiness",
            "name": "Akhyar Maulana",
            "url": "https://akhyarmaulana.com/",
            "description": "With over a decade of experience in web development, I specialize in full-stack development, with a particular emphasis on backend technologies",
            "logo": "https://akhyarmaulana.com/assets/img/logo-unusual-negative.png",
            "sameAs": "https://www.instagram.com/pangeranweb/",
            "openingHours": "Mo-Fr 10:00-17:00",
            "address": {
                "@type": "PostalAddress",
                "addressCountry": "Indonesia",
                "postalCode": "16810",
                "streetAddress": "ITC Fatmawati Blok B1 no 24, Jakarta"
            },
            "contactPoint": {
                "@type": "ContactPoint",
                "telephone": "+6289671304121",
                "contactType": "customer service",
                "contactOption": "TollFree",
                "email": "admin@akhyarmaulana.com",
                "areaServed": "ID",
                "availableLanguage": "Indonesian"
            }
        }
        </script>

    <?php endif; ?>
    
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-CL3GSPX9E8"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-CL3GSPX9E8');
    </script>
  </head>
  <body>