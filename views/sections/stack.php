<section class="fluid-container tecnology-fluid-container" id="stack">
    <section id="tecnologies" class="container mt-4 mb-4">
        <div class="row mt-md-5">
            <div class="col-md-4 d-none d-sm-none d-md-block d-lg-block">
                <h3 class="text-white" data-aos="fade-down" data-aos-duration="4000" data-aos-delay="500">My Tech Stack</h3>
                <div class="title-underline-secondary" style="margin:0 0" data-aos="fade-down" data-aos-duration="5000" data-aos-delay="800"></div>
                <p class="text-white mt-4" data-aos="fade-down" data-aos-duration="5000" data-aos-delay="1200">
                    <?=$stack['info']?>
                </p>
            </div>
            <div class="col-12 d-block d-sm-block d-md-none d-lg-none">
                <h3 class="text-white text-center" data-aos="fade-down" data-aos-duration="4000" data-aos-delay="500">My Tech Stack</h3>
                <div class="title-underline-secondary m-auto" style="margin:0 0" data-aos="fade-down" data-aos-duration="5000" data-aos-delay="800"></div>
                <p class="text-white mt-4 text-center" data-aos="fade-down" data-aos-duration="5000" data-aos-delay="1200">
                    <?=$stack['info_small']?>
                </p>
            </div>
            <?php
            $picture = $stack['pictures'];
            ?>
            
            <div class="col-12 col-md-8 d-none d-sm-none d-md-block d-lg-block">
                <div id="tech-carousel" class="carousel slide container px-0" data-bs-ride="carousel" data-interval="2000"  data-aos="fade-down" data-aos-duration="5000" data-aos-delay="1500">
                    <div class="carousel-indicators">
                        <?php foreach ($picture as $k => $v) : ?>
                        <button type="button" data-bs-target="#tech-carousel" data-bs-slide-to="<?= $k ?>" class="<?= $k == 0 ? "active" : "" ?>" aria-current="<?= $k == 0 ? "true" : "" ?>"></button>
                        <?php endforeach; ?>
                    </div>
                    <div class="carousel-inner w-100">
                        <?php foreach ($picture as $k => $v) : ?>
                        <div class="carousel-item remove-border <?= $k == 0 ? "active" : "" ?>">
                            <div class="col-md-3">
                                <div class="card card-body remove-border tech-card">
                                    <img class="img-fluid med-rounded tech-carousel-img" src="<?= $v ?>">
                                </div>
                            </div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            
            <div class="col-12 d-block d-sm-block d-md-none d-lg-none">
                <div class="swiper-tech">
                    <div class="swiper-wrapper">
                    <?php foreach ($picture as $k => $v) : ?>
                        <div class="swiper-slide">
                        <div class="card card-body remove-border tech-card">
                            <img class="img-fluid med-rounded tech-carousel-img" src="<?= $v ?>">
                        </div>
                        </div>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>