<section class="case-study-container fluid-container mt-5 pt-3 pb-5" id="educations">
    <section id="study-case" class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-12">
                <h3 class="text-center text-white my-4" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="400">My Educations</h3>
                <div class="title-underline-secondary mb-5" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="700"></div>
            </div>
            <div class="col-12 col-sm-12 col-md-6">
                <?php foreach ($educations as $v) : ?>
                <div class="col-md-12">
                    <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="700">
                        <div class="card-body case-study-card-body full-border-radius">
                            <!-- <small class="text-primary"><?=$v['content']['education_time']?></small> -->
                            <h5 class="card-title"><?=$v['page_value']?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?=$v['content']['education_desc']?></h6>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
</section>