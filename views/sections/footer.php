<footer class="container-fluid footer-container footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-12 footer-col">
                <div class="footer-logo">
                    <a class="navbar-brand" href="<?=uri('')?>">
                        <img src="<?=uri('assets/img/logo-unusual-negative.png')?>" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="50" alt="Akhyar Maulana Logo" class="w-100 footer-main-logo">
                    </a>
                </div>
                <div class="footer-address" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="100">
                    <p><?=$setting['address']?></p>
                </div>
            </div>
            <div class="col-md-3 col-6 footer-col footer-contacts">
                <h5 data-aos="fade-right" data-aos-duration="4000" data-aos-delay="150">Contacts</h5>
                <ul class="footer-menu">
                    <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="250"><a href="https://wa.me/<?=$setting['phone']?>"><i class="fa fa-whatsapp"></i>&nbsp; +<?=$setting['phone']?></a></li>
                    <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="300"><a href="mailto:<?=$setting['email']?>"><i class="fa fa-email"></i>&nbsp;<?=$setting['email']?></a></li>
                </ul>
            </div>
            <div class="col-md-3 col-6 footer-col">
                <h5 data-aos="fade-right" data-aos-duration="4000" data-aos-delay="350">Quick Access</h5>
                <ul class="footer-menu">
                    <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="400"><a href="#bio">Bio</a></li>
                    <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="450"><a href="#stack">Stack</a></li>
                    <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="500"><a href="#my-works">Works</a></li>
                    <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="550"><a href="#experience">Experience</a></li>
                    <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="550"><a href="#education">Education</a></li>
                </ul>
            </div>
            <div class="col-md-3 col-12 footer-col">
                <h5 data-aos="fade-right" data-aos-duration="4000" data-aos-delay="600">Follow Us</h5>
                <div class="footer-social-icons">
                    <a data-aos="fade-right" data-aos-duration="4000" data-aos-delay="700" href="<?=$setting['linkedin']?>" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    <a data-aos="fade-right" data-aos-duration="4000" data-aos-delay="900" href="<?=$setting['instagram']?>" class="instagram"><i class="fa fa-instagram"></i></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <p class="copy-right-footer">© 2024 All rights reserved - Akhyar Maulana</p>
            </div>
        </div>
    </div>
</footer>