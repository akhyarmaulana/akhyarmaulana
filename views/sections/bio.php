<section class="container-fluid dot-pattern" id="bio">
    <section class="container about-title-container">
        <div class="row justify-content-center">
            <div class="col-12 col-sm-12 col-md-6">
                <div class="about-main-title">
                    <h1 class="aboutus-title" id="bio" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="300">My Bio</h1>
                    <div class="title-underline-secondary" style="margin:0 0" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="300"></div>
                    <div class="row">
                        <div class="col-md-5 col-12 pt-5 px-4">
                            <img src="<?=uri($bio['bio_photo']['page_value'])?>" alt="About Banner" class="about-main-image float-left d-inline w-100" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="700">
                        </div>
                        <div class="col-md-7 col-12 pt-4">
                            <p class="main-pharagraph" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="500">
                                <?=$bio['bio_main_phar']['page_value']?>
                            </p>        
                        </div>
                    </div>
                    <p class="main-pharagraph" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="500">
                        <?=$bio['bio_secondary_phar']['page_value']?>
                    </p>
                    
                </div>
            </div>
        </div>
    </section>
</section>