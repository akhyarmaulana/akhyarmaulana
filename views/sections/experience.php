<section class="fluid-container news-container dot-pattern" id="experience">
    <section id="whats-new" class="container mt-md-4">
        <div class="row my-md-5">
            
            <div class="col-12 mb-4">
                <h3 class="text-center" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="800">My Experience</h3>
                <div class="title-underline-secondary m-auto" style="margin:0 0" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="1200"></div>
            </div>
            
        </div>

        
        <div class="row d-none d-sm-none d-md-flex d-lg-flex">
            <?php foreach($experience as $k => $v) : ?>
                <?php 
                if ($k%2 == 0) {
                    continue;
                }
                ?>
            <div class="col-12 col-md-6">
                <div class="article-card half med-rounded article-small-card" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="1000">
                    <div class="row g-0">
                        <div class="col-12 col-md-5">
                            <img src="<?=uri($v['content']['job_image']) ?>" class="card-img-left article-small-img w-100" alt="<?=$v['page_value']?>">
                        </div>
                        <div class="col-12 col-md-7">
                            <div class="article-card-body p-4">
                                <small class="article-card-category"><?=$v['content']['job_category']?></small>
                                <h5 class="article-card-title"><?=$v['page_value']?></h5>
                                <small class="d-block mb-2"><i class="fa fa-clock"></i><?=$v['content']['job_time']?></small>
                                <p class="article-card-sort-desc"><?=$v['content']['job_desc']?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
            <?php foreach($experience as $k => $v) : ?>
            <?php 
            if ($k%2 != 0) {
                continue;
            }
            ?>
            <div class="col-12 col-md-6">
                <div class="article-card half med-rounded article-small-card" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="1000">
                    <div class="row g-0">
                        <div class="col-12 col-md-5">
                            <img src="<?=uri($v['content']['job_image']) ?>" class="card-img-left article-small-img w-100" alt="<?=$v['page_value']?>">
                        </div>
                        <div class="col-12 col-md-7">
                            <div class="article-card-body p-4">
                                <small class="article-card-category"><?=$v['content']['job_category']?></small>
                                <h5 class="article-card-title"><?=$v['page_value']?></h5>
                                <small class="d-block mb-2"><i class="fa fa-clock"></i><?=$v['content']['job_time']?></small>
                                <p class="article-card-sort-desc"><?=$v['content']['job_desc']?></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </div>

        <div class="row d-flex d-sm-flex d-md-none d-lg-none">
            <div class="col-12">
                <div class="swiper-news">
                    <div class="swiper-wrapper">
                        <?php foreach($experience as $k => $v) : ?>
                        <div class="swiper-slide">
                        <div class="article-card med-rounded">
                            <img src="<?=uri($v['content']['job_image']) ?>" class="card-img-top article-img" alt="<?=$v['page_value']?>">
                            <div class="article-card-body p-4">
                                <small class="article-card-category"><?=$v['content']['job_category']?></small>
                                <h5 class="article-card-title"><?=$v['page_value']?></h5>
                                <small class="d-block mb-2"><i class="fa fa-clock"></i><?=$v['content']['job_time']?></small>
                                <p class="article-card-sort-desc"><?=$v['content']['job_desc']?></p>
                                
                            </div>
                        </div>
                        </div>
                        <?php endforeach; ?>
                        
                    </div>
                </div>
            </div>
        </div>

        
        
    </section>
</section>