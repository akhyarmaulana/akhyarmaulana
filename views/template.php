<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="apple-touch-icon" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-chrome-192x192.png">
    <link rel="icon" type="image/png" sizes="512x512"  href="/android-chrome-512x512.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="manifest.json">


    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"/>
    <link rel="stylesheet" href="assets/css/about.css">
    <link rel="stylesheet" href="assets/css/style.css">
    
    <title>Akhyar Maulana | Software Solution</title>
    <meta name="title" content="Akhyar Maulana | Solusi Software, Website, dan Aplikasi Mobile">
    <meta name="description" content="Solusi pembuatan software, mobile, aplikasi, dan website dengan harga yang bersahabat">
    <meta name="keywords" content="jasa programmer, jasa website, pembuatan website, jasa aplikasi, pembuatan aplikasi, app developer, web developer, system developer">
    <meta property="og:title" content="Akhyar Maulana | Solusi Software, Website, dan Aplikasi Mobile">
    <meta property="og:description" content="Solusi pembuatan software, mobile, aplikasi, dan website berkualitas dengan harga yang bersahabat">
    <meta property="og:image" content="assets/img/table-setup.jpg">
    <meta property="og:url" content="" />
    <meta name="twitter:card" content="summary_large_image" />
    <meta name="twitter:title" content="Akhyar Maulana | Solusi Software, Website, dan Aplikasi Mobile" />
    <meta name="twitter:description" content="Solusi pembuatan software, mobile, aplikasi, dan website berkualitas dengan harga yang bersahabat" />
    <meta name="twitter:image" content="assets/img/table-setup.jpg" />
    <meta name="twitter:image:src" content="assets/img/table-setup.jpg" /> 


    <meta name="robots" content="index, follow" />
    <meta name="googlebot" content="index, follow" />
    <meta name="googlebot-news" content="index, follow" />
    <meta name="google-site-verification" content=""/>
    <link rel="dns-prefetch" href="//connect.facebook.net/">
    <link rel="preconnect" href="//connect.facebook.net/" crossorigin>
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="mobile-web-app-capable" content='yes' >
    <meta name="apple-touch-fullscreen" content='yes' >
    <meta name="apple-mobile-web-app-capable" content='yes' >
    <meta name="apple-mobile-web-app-status-bar-style" content='default' >
    <meta name="theme-color" content="#0C9344">
    <link rel="canonical" href="https://akhyarmaulana.com" >

    <script type="application/json">
        "@context": "http://schema.org",
        "@type": "LocalBusiness",
        "name": "Akhyar Maulana",
        "url": "https://akhyarmaulana.com/",
        "description": "Solusi pembuatan software, mobile, aplikasi, dan website dengan harga yang bersahabat. Siap mendukung usaha anda menjadi lebih baik.",
        "logo": "assets/img/logo-unusual-negative.png",
        "SameAs": "https://www.instagram.com/pangeranweb/",
        "OpeningHours": "Mo-Fr 10:00-17:00",
        "Address": {
            "@type": "PostalAddress",
            "addressCountry": "Indonesia",
            "postalCode": "16810",
            "streetAddress": "ITC Fatmawati Blok B1 no 24, Jakarta",
        }
        "contactPoint": [
            "@type": "ContactPoint".
            "telephone": " 6289671304121",
            "contactType": "customer service",
            "contactOption": "TollFree",
            "email": "admin@akhyarmaulana.com",
            "areaServed": "ID",
            "availableLanguage": "Indonesia"
        ]
    </script>

    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-CL3GSPX9E8"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'G-CL3GSPX9E8');
    </script>
    
  </head>
  <body>
    
    <header class="container-fluid main-header">
    <div class="container center-main-header">
        <nav class="navbar navbar-expand-lg">
            <div class="container-fluid">
              <a class="navbar-brand" href="/">
                <img src="assets/img/logo-unusual-negative.png" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="50" alt="Akhyar Maulana Logo" class="w-100 main-logo">
              </a>
              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="fa fa-navicon text-white"></span>
              </button>
              <div class="collapse navbar-collapse main-navbar" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="500">
                    <a class="nav-link active" aria-current="page" href="#bio">Bio</a>
                  </li>
                  <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="700">
                    <a class="nav-link" href="#stack">Stack</a>
                  </li>
                  <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="900">
                    <a class="nav-link " href="#my-works">Works</a>
                  </li>
                  <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1200">
                    <a class="nav-link " href="#experience">Experience</a>
                  </li>
                  <li class="nav-item" data-aos="fade-left" data-aos-duration="4000" data-aos-delay="1400">
                    <a class="nav-link" href="#educations">Education</a>
                  </li>
                </ul>
                <div data-aos="fade-left" data-aos-duration="4000" data-aos-delay="4000" class="action-button">
                    <a class="btn btn-warning my-bg-secondary full-round font-bold" href="https://wa.me/6289671304121"><i class="fa fa-whatsapp"></i> WhatsApp Me</a>
                </div>
              </div>
            </div>
          </nav>
        </div>
    </header>    

    <section class="container-fluid dot-pattern" id="bio">
        <section class="container about-title-container">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="about-main-title">
                        <h1 class="aboutus-title" id="bio" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="300">My Bio</h1>
                        <div class="title-underline-secondary" style="margin:0 0" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="300"></div>
                        <div class="row">
                            <div class="col-5 pt-5 px-4">
                                <img src="assets/img/profile-picture.jpeg" alt="About Banner" class="about-main-image float-left d-inline" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="700">
                            </div>
                            <div class="col-7 pt-4">
                                <p class="main-pharagraph" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="500">
                                    With over a decade of experience in web development, I specialize in full-stack development, with a particular emphasis on backend technologies such as Laravel and Go. My expertise in these areas has allowed me to create robust and efficient web applications that meet the needs of clients and users alike. 
                                </p>        
                            </div>
                        </div>
                        <p class="main-pharagraph" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="500">
                            Through my extensive experience, I've honed my skills in crafting scalable and secure backend architectures, ensng that the applications I build are both reliable and high-performing.
                        </p>
                        
                    </div>
                </div>
            </div>
        </section>
    </section>

    <section class="fluid-container tecnology-fluid-container" id="stack">
        <section id="tecnologies" class="container mt-4 mb-4">
            <div class="row mt-md-5">
                <div class="col-md-4 d-none d-sm-none d-md-block d-lg-block">
                    <h3 class="text-white" data-aos="fade-down" data-aos-duration="4000" data-aos-delay="500">My Tech Stack</h3>
                    <div class="title-underline-secondary" style="margin:0 0" data-aos="fade-down" data-aos-duration="5000" data-aos-delay="800"></div>
                    <p class="text-white mt-4" data-aos="fade-down" data-aos-duration="5000" data-aos-delay="1200">
                    Whether it's for mobile, web, or other platforms, this software enables me to bring my ideas to life and develop innovative solutions for various purposes. Its flexibility and robust features allow me to tailor each app to meet specific needs, ensng they're both functional and visually appealing.
                    </p>
                </div>
                <div class="col-12 d-block d-sm-block d-md-none d-lg-none">
                    <h3 class="text-white text-center" data-aos="fade-down" data-aos-duration="4000" data-aos-delay="500">My Tech Stack</h3>
                    <div class="title-underline-secondary m-auto" style="margin:0 0" data-aos="fade-down" data-aos-duration="5000" data-aos-delay="800"></div>
                    <p class="text-white mt-4 text-center" data-aos="fade-down" data-aos-duration="5000" data-aos-delay="1200">
                        The software that I use to build a lot of cool Apps.
                    </p>
                </div>
                <?php
                $picture = [
                    ('assets/img/flutter-tech.png'),
                    ('assets/img/docker-tech.png'),
                    ('assets/img/laravel-tech.png'),
                    ('assets/img/mysql-tech.png'),
                    ('assets/img/postgre-tech.png'),
                    ('assets/img/redis-tech.png'),
                    ('assets/img/go-tech.png'),
                ];
                ?>
                
                <div class="col-12 col-md-8 d-none d-sm-none d-md-block d-lg-block">
                    <div id="tech-carousel" class="carousel slide container px-0" data-bs-ride="carousel" data-interval="2000"  data-aos="fade-down" data-aos-duration="5000" data-aos-delay="1500">
                        <div class="carousel-indicators">
                            <?php foreach ($picture as $k => $v) : ?>
                            <button type="button" data-bs-target="#tech-carousel" data-bs-slide-to="<?= $k ?>" class="<?= $k == 0 ? "active" : "" ?>" aria-current="<?= $k == 0 ? "true" : "" ?>"></button>
                            <?php endforeach; ?>
                        </div>
                        <div class="carousel-inner w-100">
                            <?php foreach ($picture as $k => $v) : ?>
                            <div class="carousel-item remove-border <?= $k == 0 ? "active" : "" ?>">
                                <div class="col-md-3">
                                    <div class="card card-body remove-border tech-card">
                                        <img class="img-fluid med-rounded tech-carousel-img" src="<?= $v ?>">
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                
                <div class="col-12 d-block d-sm-block d-md-none d-lg-none">
                    <div class="swiper-tech">
                        <div class="swiper-wrapper">
                        <?php foreach ($picture as $k => $v) : ?>
                          <div class="swiper-slide">
                            <div class="card card-body remove-border tech-card">
                                <img class="img-fluid med-rounded tech-carousel-img" src="<?= $v ?>">
                            </div>
                          </div>
                        <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    <img src="assets/img/table-setup.jpg" class="fix-parralax" >

    <section class="case-study-container fluid-container mt-5 pt-3 pb-5" id="my-works">
        <section id="study-case" class="container mt-4">
            <div class="row">
                <div class="col-12">
                    <h3 class="text-center text-white my-4" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="400">My Recent Works</h3>
                    <div class="title-underline-secondary mb-5" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="700"></div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="col-md-12">
                        <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="700">
                            <img src="<?=('assets/img/ticketing.png') ?>" class="card-img-top" alt="Pangeran Web - Case Study - Bus Ticketing">
                            <div class="card-body case-study-card-body">
                                <small class="text-primary">Transportation</small>
                                <h5 class="card-title">Bus Ticket Web & Apps</h5>
                                <h6 class="card-subtitle mb-2 text-muted">An integrated bus ticket booking application with multiple pickup points in each city</h6>
                                <p class="card-text">
                                    
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="1000">
                            <img src="<?=('assets/img/e-commerce-study-case.png') ?>" class="card-img-top" alt="Pangeran Web | Point of Sales">
                            <div class="card-body case-study-card-body">
                                <small class="text-primary">E-Commerce</small>
                                <h5 class="card-title">Marketplace Web & Apps</h5>
                                <h6 class="card-subtitle mb-2 text-muted">A marketplace application where sellers and buyers can interact. It's connected to a payment gateway and has automated shipping.</h6>
                                <p class="card-text">
                                    
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="col-md-12">
                        <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="1300">
                            <img src="<?=('assets/img/e-learning-study-case.png') ?>" class="card-img-top" alt="Pangeran Web | Point of Sales">
                            <div class="card-body case-study-card-body">
                                <small class="text-primary">E-Learning</small>
                                <h5 class="card-title">Elearning Class</h5>
                                <h6 class="card-subtitle mb-2 text-muted">A membership (subscription) application for viewing instructional videos and taking quizzes</h6>
                                <p class="card-text">
                                    
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="10000" data-aos-delay="1600">
                            <img src="<?=('assets/img/system-information-study-case.png') ?>" class="card-img-top" alt="Pangeran Web | Point of Sales">
                            <div class="card-body case-study-card-body">
                                <small class="text-primary">Information System</small>
                                <h5 class="card-title">Management Stok & Distribution</h5>
                                <h6 class="card-subtitle mb-2 text-muted">An agent booking application connected to factories and integrated with existing ERP systems like ASP, for example.</h6>
                                <p class="card-text">
                                    
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </section>

    



    <section class="fluid-container news-container dot-pattern" id="experience">
        <section id="whats-new" class="container mt-md-4">
            <div class="row my-md-5">
                
                <div class="col-12 mb-4">
                    <h3 class="text-center" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="800">My Experience</h3>
                    <div class="title-underline-secondary m-auto" style="margin:0 0" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="1200"></div>
                </div>
                
            </div>

            
            <div class="row d-none d-sm-none d-md-flex d-lg-flex">
                <div class="col-12 col-md-6">
                    <div class="article-card half med-rounded article-small-card" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="1000">
                        <div class="row g-0">
                            <div class="col-12 col-md-5">
                                <img src="assets/img/dotcom-work.jpeg" class="card-img-left article-small-img w-100" alt="Dotcom Solutions as CTO">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="article-card-body p-4">
                                    <small class="article-card-category">Software House</small>
                                    <h5 class="article-card-title">Dotcom Solutions as CTO</h5>
                                    <small class="d-block mb-2"><i class="fa fa-clock"></i>2019 - Present (2024)</small>
                                    <p class="article-card-sort-desc">We are working app and website orders from clients, ensuring their specifications are met with precision.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 col-md-6">
                    <div class="article-card half med-rounded article-small-card" data-aos="fade-up" data-aos-duration="4000" data-aos-delay="1000">
                        <div class="row g-0">
                            <div class="col-12 col-md-5">
                                <img src="assets/img/evista-work.jpeg" class="card-img-left article-small-img w-100" alt="Dotcom Solutions as CTO">
                            </div>
                            <div class="col-12 col-md-7">
                                <div class="article-card-body p-4">
                                    <small class="article-card-category">Electric Taxi</small>
                                    <h5 class="article-card-title">Evista as CTO</h5>
                                    <small class="d-block mb-2"><i class="fa fa-clock"></i>2023 - Present (2024)</small>
                                    <p class="article-card-sort-desc">We operate an electric airport taxi service, emphasizing cutting-edge taxi technology and providing a seamless electric vehicle experience for our customers. Our focus is on integrating advanced technology into the taxi industry while delivering a comfortable and eco-friendly transportation solution.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row d-flex d-sm-flex d-md-none d-lg-none">
                <div class="col-12">
                    <div class="swiper-news">
                        <div class="swiper-wrapper">
                          <!-- @foreach (['Save Your Database with Enscription', 'Daily Backup is The Best Disaster Management','Save Your Database with Enscription', 'Daily Backup is The Best Disaster Management'] as $k => $v) -->
                          <div class="swiper-slide">
                            <div class="article-card med-rounded">
                                <img src="assets/img/dotcom-work.jpeg" class="card-img-top article-img" alt="">
                                <div class="article-card-body p-4">
                                    <small class="article-card-category">Software House</small>
                                    <h5 class="article-card-title">Dotcom Solutions as CTO</h5>
                                    <small class="d-block mb-2"><i class="fa fa-clock"></i>2019 - Present (2024)</small>
                                    <p class="article-card-sort-desc">We are working app and website orders from clients, ensuring their specifications are met with precision.</p>
                                    
                                </div>
                            </div>
                          </div>
                          <div class="swiper-slide">
                            <div class="article-card med-rounded">
                                <img src="assets/img/dotcom-work.jpeg" class="card-img-top article-img" alt="">
                                <div class="article-card-body p-4">
                                    <small class="article-card-category">Software House</small>
                                    <h5 class="article-card-title">Dotcom Solutions as CTO</h5>
                                    <small class="d-block mb-2"><i class="fa fa-clock"></i>2019 - Present (2024)</small>
                                    <p class="article-card-sort-desc">We are working app and website orders from clients, ensuring their specifications are met with precision.</p>
                                    
                                </div>
                            </div>
                          </div>
                          <div class="swiper-slide">
                            <div class="article-card med-rounded">
                                <img src="assets/img/dotcom-work.jpeg" class="card-img-top article-img" alt="">
                                <div class="article-card-body p-4">
                                    <small class="article-card-category">Software House</small>
                                    <h5 class="article-card-title">Dotcom Solutions as CTO</h5>
                                    <small class="d-block mb-2"><i class="fa fa-clock"></i>2019 - Present (2024)</small>
                                    <p class="article-card-sort-desc">We are working app and website orders from clients, ensuring their specifications are met with precision.</p>
                                    
                                </div>
                            </div>
                          </div>
                          <!-- @endforeach -->
                        </div>
                    </div>
                </div>
            </div>

            
            
        </section>
    </section>



    <section class="case-study-container fluid-container mt-5 pt-3 pb-5" id="educations">
        <section id="study-case" class="container mt-4">
            <div class="row justify-content-center">
                <div class="col-12">
                    <h3 class="text-center text-white my-4" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="400">My Educations</h3>
                    <div class="title-underline-secondary mb-5" data-aos="fade-up" data-aos-duration="5000" data-aos-delay="700"></div>
                </div>
                <div class="col-12 col-sm-12 col-md-6">
                    <div class="col-md-12">
                        <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="700">
                            <div class="card-body case-study-card-body full-border-radius">
                                <small class="text-primary">May 2022 - Present</small>
                                <h5 class="card-title">Universitas Mercu Buana Yogyakarta - Bachelor</h5>
                                <h6 class="card-subtitle mb-2 text-muted">Computer Science - Artificial Intelligence </h6>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="900">
                            
                            <div class="card-body case-study-card-body full-border-radius">
                                <small class="text-primary">2013 - 2015</small>
                                <h5 class="card-title">MIT Bootcamp - Backend Developer</h5>
                                <h6 class="card-subtitle mb-2 text-muted">The Study is focusing on hi-trafic backend API development.</h6>
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="card w-100 case-study-card" data-aos="fade-up" data-aos-duration="8000" data-aos-delay="1000">
                            
                            <div class="card-body case-study-card-body full-border-radius">
                                <small class="text-primary">2010 - 2013</small>
                                <h5 class="card-title">Sekolah Menengah Kejuruan 3 (Vocational High School 3) Yogyakarta</h5>
                                <h6 class="card-subtitle mb-2 text-muted">The Study is focusing on web development and networking.</h6>
                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </section>
    </section>

    

    <footer class="container-fluid footer-container footer">
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-12 footer-col">
                    <div class="footer-logo">
                        <a class="navbar-brand" href="http://localhost/">
                            <img src="assets/img/logo-unusual-negative.png" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="50" alt="Akhyar Maulana Logo" class="w-100 footer-main-logo">
                        </a>
                    </div>
                    <div class="footer-address" data-aos="fade-right" data-aos-duration="4000" data-aos-delay="100">
                        <p>Kompleks Duta Fatmawati Lt. 2 Blok B No. 24, RT.1/RW.5, North Cipete, Kebayoran Baru, South Jakarta City, Jakarta 55198</p>
                    </div>
                </div>
                <div class="col-md-3 col-6 footer-col footer-contacts">
                    <h5 data-aos="fade-right" data-aos-duration="4000" data-aos-delay="150">Contacts</h5>
                    <ul class="footer-menu">
                        <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="200"><a href="tel:+6289671304121"><i class="fa fa-phone"></i>&nbsp;+6289-671-304-121</a></li>
                        <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="250"><a href="https://wa.me/6289671304121"><i class="fa fa-whatsapp"></i>&nbsp; +6289-671-304-121</a></li>
                        <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="300"><a href="mailto:info@akhyarmaulana.com"><i class="fa fa-email"></i>&nbsp;me@akhyarmaulana.com</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-6 footer-col">
                    <h5 data-aos="fade-right" data-aos-duration="4000" data-aos-delay="350">Quick Access</h5>
                    <ul class="footer-menu">
                        <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="400"><a href="#bio">Bio</a></li>
                        <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="450"><a href="#stack">Stack</a></li>
                        <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="500"><a href="#my-works">Works</a></li>
                        <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="550"><a href="#experience">Experience</a></li>
                        <li data-aos="fade-right" data-aos-duration="4000" data-aos-delay="550"><a href="#education">Education</a></li>
                    </ul>
                </div>
                <div class="col-md-3 col-12 footer-col">
                    <h5 data-aos="fade-right" data-aos-duration="4000" data-aos-delay="600">Follow Us</h5>
                    <div class="footer-social-icons">
                        <a data-aos="fade-right" data-aos-duration="4000" data-aos-delay="700" href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                        <a data-aos="fade-right" data-aos-duration="4000" data-aos-delay="800" href="#" class="youtube"><i class="fa fa-youtube"></i></a>
                        <a data-aos="fade-right" data-aos-duration="4000" data-aos-delay="900" href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <p class="copy-right-footer">© 2024 All rights reserved - Akhyar Maulana</p>
                </div>
            </div>
        </div>
    </footer>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js'></script>
    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
    <script>
    $('.carousel .carousel-item').each(function () {
        var minPerSlide = 4
        var next = $(this).next();
        if (!next.length) {
            next = $(this).siblings(':first');
        }
        next.children(':first-child').clone().appendTo($(this));

        for (var i = 0; i < minPerSlide; i++) {
            next = next.next();
            if (!next.length) {
                next = $(this).siblings(':first');
            }
            next.children(':first-child').clone().appendTo($(this));
        }
    });
    </script>
    <script>
        var height_windows = ($(window).height());
        var width_window = ($(window).width());
        
        if (width_window > 990) {
            $("#banner").css("height", height_windows + "px");
            $(".super-title").css("margin-top", ((height_windows / 2 ) - 200) + "px");
        }
        
        
    </script>
    <script>
        var height = $(".article-small-card").height();
        $(".card-img-left").css("height",height + "px");
    </script>
    <script>
        let prevScrollPos = window.pageYOffset;
    
        window.onscroll = function() {
        const currentScrollPos = window.pageYOffset;
    
        if (prevScrollPos > currentScrollPos) {
            // Scrolling up
            $(".main-header").removeClass("hidden");
        } else {
            // Scrolling down
            $(".main-header").addClass("hidden");
        }
    
        prevScrollPos = currentScrollPos;
        };
    
    </script>
    <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js"></script>
    <script>
            
            const swiper = new Swiper('.swiper', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
                slidesPerView: 1.5,
                breakpoints: {
                    320: {
                        spaceBetween: 2,
                        spaceBetween: 20,
                    },
                    720: {
                        slidesPerView: 2,
                        spaceBetween: 20,
                    },
                    992: {
                        slidesPerView: 4,
                        spaceBetween: 30,
                    },
                    2250: {
                        slidesPerView: 4,
                        spaceBetween: 30,
                    },
                }
            });

            const swipertech = new Swiper('.swiper-tech', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
                slidesPerView: 3.5,
                breakpoints: {
                    320: {
                        spaceBetween: 4,
                        spaceBetween: 0,
                    },
                    720: {
                        slidesPerView: 4,
                        spaceBetween: 0,
                    },
                    992: {
                        slidesPerView: 4,
                        spaceBetween: 0,
                    },
                    2250: {
                        slidesPerView: 4,
                        spaceBetween: 0,
                    },
                }
            });            

            const swipernews = new Swiper('.swiper-news', {
                // Optional parameters
                direction: 'horizontal',
                loop: true,
                slidesPerView: 1.3,
                breakpoints: {
                    320: {
                        spaceBetween: 20,
                        spaceBetween: 20,
                    },
                    720: {
                        slidesPerView: 20,
                        spaceBetween: 20,
                    },
                    992: {
                        slidesPerView: 30,
                        spaceBetween: 30,
                    },
                    2250: {
                        slidesPerView: 30,
                        spaceBetween: 30,
                    },
                }
            });            

    </script>
  </body>
</html>